
// Get the header
var header = document.getElementById("header");
var naviList = document.querySelector('.navi').children;
var socialIcons = document.querySelector('.social-icos').children;
var techIcons = document.querySelector('.techs').children;
var projects = document.querySelector('.grid-projects-wrapper').children;
var posts = document.querySelector('.grid-posts-wrapper').children;
var cards = document.querySelector('.cards').children;



let elementList = document.querySelectorAll("section");
elementList.forEach(element => {
    element.setAttribute("data-aos","fade-up");
    element.setAttribute("data-aos-duration","1000");
});
Array.prototype.forEach.call(techIcons,element =>{
  element.setAttribute("data-aos","fade-left");
  element.setAttribute("data-aos-duration","1000");
});

Array.prototype.forEach.call(projects,element =>{
  element.setAttribute("data-aos","fade-left");
  element.setAttribute("data-aos-duration","1000");
})

Array.prototype.forEach.call(posts,element =>{
  element.setAttribute("data-aos","fade-left");
  element.setAttribute("data-aos-duration","1000");
})

Array.prototype.forEach.call(cards,element =>{
  element.setAttribute("data-aos","fade-left");
  element.setAttribute("data-aos-duration","1000");
})



window.onscroll = function() {
    
    
    myFunction()
};



// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    
    header.classList.add("sticky");
    //navi.classList.add("sticky-nav");
    Array.prototype.forEach.call(naviList,element=>{
        element.firstChild.classList.add('sticky-nav');
    });
    Array.prototype.forEach.call(socialIcons,element=>{
      element.classList.add('sticky-nav');
  });
  } else {
    header.classList.remove("sticky");
    //navi.classList.add("sticky-nav");
    Array.prototype.forEach.call(naviList,element=>{
        element.firstChild.classList.remove('sticky-nav');
    });
    Array.prototype.forEach.call(socialIcons,element=>{
      element.classList.remove('sticky-nav');
  });
  }
}






